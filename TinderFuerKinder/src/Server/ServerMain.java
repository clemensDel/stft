package Server;

import javax.swing.JFrame;

public class ServerMain extends JFrame{
	
	
	private static final long serialVersionUID = 1L;
	
	private int port; //Port = 8192
	private Server server;
	
	public ServerMain(int port) {
		this.port = port;
		server = new Server(port);
	}
	
	public static void main(String[] args) {
		int port;
		if(args.length != 1) {
			System.out.println("Usage: java -jar  Server.jar[Port]");
			return;
		}
		port = Integer.parseInt(args[0]);
		new ServerMain(port);
		
	}

}
