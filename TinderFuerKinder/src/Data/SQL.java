package Data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import Cofig.ConfigManager;

public abstract class SQL {
	
	public static Connection getConnection() throws NumberFormatException, SQLException{
		return DriverManager.getConnection("jdbc:mysql://"+ConfigManager.get("DB_HOST")+":"+Integer.parseInt(ConfigManager.get("DB_PORT"))+"/?user="+ConfigManager.get("DB_USER")+"&password="+ConfigManager.get("DB_PASSWORD"));
		
	}
	
}
