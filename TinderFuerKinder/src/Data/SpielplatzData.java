package Data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import Cofig.ConfigManager;

public class SpielplatzData extends SQL {

	public static ArrayList<String> getAllSpielplatz() {
		Connection con = null;
		Statement stmt = null;
		ArrayList<String> playland = new ArrayList<>();
		
		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog(ConfigManager.get("DB_DATABASE"));
			
			String query = "SELECT name FROM T_Spielplatz;";
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while(rs.next()) {
				playland.add(rs.getString(1));
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return playland;
		
	}
}
