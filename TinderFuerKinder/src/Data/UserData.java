package Data;

import java.awt.Image;
import java.sql.*;
import java.util.*;

import javax.swing.plaf.synth.SynthSplitPaneUI;

import Logic.*;
import Cofig.ConfigManager;

public class UserData extends SQL {

	public static void addUser(User user) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog(ConfigManager.get("DB_DATABASE"));
			
			String query = "INSERT INTO T_Users ('P_User_name','vorname','name','augenfarbe','haarfarbe','hobby1',"
					+ "'hobby2','hobby3',"
					+ "'spielplatz1','spielplatz2','spielplatz3','password','salt') VAULES "
					+ "(?,?,?,?,?,?,?,?,?,?,?,?,?);";
			
			
			
			stmt = con.prepareStatement(query);
			
			stmt.setString(1, user.getUsername());
			stmt.setString(2, user.getVorname());
			stmt.setString(3, user.getName());
			stmt.setString(4, user.getAugenfarbe());
			stmt.setString(5, user.getHaarfarbe());
			stmt.setString(6, user.getHobby1());
			stmt.setString(7, user.getHobby2());
			stmt.setString(8, user.getHobby3());
			stmt.setString(9, user.getVorliebeSpielplatz1());
			stmt.setString(10, user.getVorliebeSpielplatz2());
			stmt.setString(11, user.getVorliebeSpielplatz3());
			stmt.setString(12, user.getPasswort());
			stmt.setString(13, user.getSalt());
			
			System.out.println(stmt);
			//stmt.executeUpdate();
			stmt.execute();
			con.commit();
			stmt.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public static void addUser1(User user) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog(ConfigManager.get("DB_DATABASE"));
			
			ArrayList<String> haarf = new ArrayList<>();
			ArrayList<String> augenf = new ArrayList<>();
			haarf = HaarfarbeData.getAllHaarfarbe();
			augenf = AugenfarbeData.getAllAugemfarbe();

			String query = "INSERT INTO T_Users "
					+ "(username,"				//1	null
					+ "vorname,"				//2	null
					+ "nachname,"				//3	null
					+ "F_Haarfarbe_Id,"			//4	null
					+ "F_Augenfarbe_Id,"		//5	null
					+ "F_Hobby_Id1,"			//6
					+ "F_Hobby_Id2,"			//7
					+ "F_Hobby_Id3,"			//8
					+ "F_Vorlieben_Haarfarbe,"	//9
					+ "F_Vorlieben_Augenfarbe,"	//10
					+ "salt,"					//11
					+ "passwort,"				//12
					+ "bild,"					//13
					+ "F_Spielplatz1,"			//14
					+ "F_Spielplatz2,"			//15
					+ "F_Spielplatz3)"			//16
					+ " VALUES("
					+ "?,"						//1
					+ "?,"						//2
					+ "?,"						//3
					+ "?,"						//4
					+ "?,"						//5
					+ "?,"						//6
					+ "?,"						//7
					+ "?,"						//8
					+ "?,"						//9
					+ "?,"						//10
					+ "?,"						//11
					+ "?,"						//12
					+ "?,"						//13
					+ "?,"						//14
					+ "?,"						//15
					+ "?);";					//16

			stmt = con.prepareStatement(query);
			stmt.setString(1, user.getUsername());
			stmt.setString(2, user.getVorname());
			stmt.setString(3, user.getName());
			int zahl = 0;
			for(int i = 0; i<haarf.size();i++) {
				if(haarf.get(i).equals(user.getHaarfarbe())) {
					zahl = i;
					zahl++;
					break;
				}
				if(i <= haarf.size()){
					zahl = 1;
				}
			}
			stmt.setInt(4, zahl);
			int zahl1 = 0;
			for(int i = 0; i<augenf.size();i++) {
				if(haarf.get(i).equals(user.getAugenfarbe())) {
					zahl1 = i;
					zahl1++;
					System.out.println(zahl1+" "+augenf.get(i));
					break;
				}
				if(i <= augenf.size()) {
					zahl1 = 1;
				}
			}
			stmt.setInt(5, zahl1);
			String hobby1String = user.getHobby1();
			int hobby1;
			if(hobby1String == null) {
				hobby1 = 11;
			}else {
				hobby1 = Integer.parseInt(hobby1String);
			}
			stmt.setInt(6, hobby1);
			String hobby2String = user.getHobby2();
			int hobby2;
			if(hobby2String == null) {
				hobby2 = 11;
			}else {
				hobby2 = Integer.parseInt(hobby1String);
			}
			stmt.setInt(7, hobby2);
			int hobby3;
			String hobby3String = user.getHobby3();
			if(hobby3String == null) {
				hobby3 = 11;
			}else {
				hobby3 = Integer.parseInt(hobby3String);
			}
			stmt.setInt(8, hobby3);
			stmt.setInt(9, 1);
			stmt.setInt(10, 1);
			String salt = user.getSalt();
			stmt.setString(11, null);
			String passwort = user.getPasswort();
			stmt.setString(12, passwort);
			stmt.setBlob(13, (Blob) user.getImage());
			stmt.setInt(14, 1);
			stmt.setInt(15, 1);
			stmt.setInt(16, 1);

			stmt.execute();
			con.commit();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public static ArrayList<User> getAllUser() {
		Connection con = null;
		Connection con1 = null;
		Statement stmt = null;
		Statement stmt1 = null;
		ArrayList<User> user = new ArrayList<>();
		ArrayList<String> hobbies = new ArrayList<>();
		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog(ConfigManager.get("DB_DATABASE"));
			
			String query = "SELECT * FROM T_Users;";
			String query1 = "SELECT hobby FROM T_Hobbies;";
			stmt = con.prepareStatement(query);
			stmt1 = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			ResultSet rs1 = stmt.executeQuery(query1);
			ArrayList<String> haarf = new ArrayList<>();
			ArrayList<String> augenf = new ArrayList<>();
			haarf = HaarfarbeData.getAllHaarfarbe();
			augenf = AugenfarbeData.getAllAugemfarbe();
			
			while(rs1.next()) {
				hobbies.add(rs1.getString(1));
			}
			
			while(rs.next()) {
				User u1 = new User();
				u1.setUsername(rs.getString("username"));
				u1.setVorname(rs.getString("vorname"));
				u1.setName(rs.getString("nachname"));
				int haar = rs.getInt("F_Haarfarbe_Id");
				haar--;
				u1.setHaarfarbe(haarf.get(haar));
				int augen = rs.getInt("F_Augenfarbe_Id");
				augen--;
				u1.setAugenfarbe(augenf.get(augen));
				int hobby1 = rs.getInt("F_Hobby_ID1");
				hobby1 = hobby1--;
				u1.setHobby1(hobbies.get(hobby1));
				int hobby2 = rs.getInt("F_Hobby_ID2");
				hobby2 = hobby2--;
				u1.setHobby2(hobbies.get(hobby2));
				int hobby3 = rs.getInt("F_Hobby_ID3");
				hobby3 = hobby3--;
				u1.setHobby3(hobbies.get(hobby3));
				haar = rs.getInt("F_Vorlieben_Haarfarbe");
				haar--;
				u1.setVorliebeHaarfarbe(haarf.get(haar));
				augen = rs.getInt("F_Vorlieben_Augenfarbe");
				augen--;
				u1.setVorliebeHaarfarbe(augenf.get(augen));
				u1.setSalt(rs.getString("salt"));
				u1.setPasswort(rs.getString("passwort"));
				u1.setInput((Image) rs.getBlob("bild"));
				//Spielplatz
				
				user.add(u1);
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return user;
		
	}

	public static User getUser(String name) {
		Connection con = null;
		Connection con1 = null;
		PreparedStatement stmt = null;
		Statement stmt1 = null;
		ArrayList<User> namen = new ArrayList<>();
		ArrayList<String> hobbies = new ArrayList<>();
		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog(ConfigManager.get("DB_DATABASE"));

			//SELECT * FROM mitarbeiter INNER JOIN abteilungen ON mitarbeiter.a_id = abteilungen.a_id;
			String query = "SELECT * FROM T_Users INNER JOIN T_Vorlieben ON T_Users.F_Vorlieben_Id = T_Vorlieben.P_Vorlieben_Id;";
			String query1 = "SELECT hobby FROM T_Hobbies;";
			stmt = con.prepareStatement(query);
			stmt1 = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			ResultSet rs1 = stmt.executeQuery(query1);
			
			ArrayList<String> haarf = new ArrayList<>();
			ArrayList<String> augenf = new ArrayList<>();
			haarf = HaarfarbeData.getAllHaarfarbe();
			augenf = AugenfarbeData.getAllAugemfarbe();
			while(rs1.next()) {
				hobbies.add(rs1.getString(1));
			}	
			
			while (rs.next()) {
				User u1 = new User();
				u1.setUsername(rs.getString("username"));
				u1.setVorname(rs.getString("vorname"));
				u1.setName(rs.getString("nachname"));
				int haar = rs.getInt("F_Haarfarbe_Id");
				haar--;
				u1.setHaarfarbe(haarf.get(haar));
				int augen = rs.getInt("F_Augenfarbe_Id");
				augen--;
				u1.setAugenfarbe(augenf.get(augen));
				int hobby1 = rs.getInt("F_Hobby_ID1");
				hobby1 = hobby1--;
				u1.setHobby1(hobbies.get(hobby1));
				int hobby2 = rs.getInt("F_Hobby_ID2");
				hobby2 = hobby2--;
				u1.setHobby2(hobbies.get(hobby2));
				int hobby3 = rs.getInt("F_Hobby_ID3");
				hobby3 = hobby3--;
				u1.setHobby3(hobbies.get(hobby3));
				haar = rs.getInt("F_Vorlieben_Haarfarbe");
				haar--;
				u1.setVorliebeHaarfarbe(haarf.get(haar));
				augen = rs.getInt("F_Vorlieben_Augenfarbe");
				augen--;
				u1.setVorliebeHaarfarbe(augenf.get(augen));
				u1.setSalt(rs.getString("salt"));
				u1.setPasswort(rs.getString("passwort"));
				u1.setInput((Image) rs.getBlob("bild"));
				namen.add(u1);
			}

			for (int i = 0; i < namen.size(); i++) {
				if ((namen.get(i).getUsername()).equals(name)) {
					return namen.get(i); // Namen vergeben
				}
			}

			return null;

		} catch (Exception e) {

		}
		return null;
	}

	public static boolean isUsernamevergeben(String name) {
		Connection con = null;
		Statement stmt = null;
		ArrayList<String> namen = new ArrayList<>();

		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog(ConfigManager.get("DB_DATABASE"));

			String query = "SELECT username FROM T_Users";

			stmt = con.prepareStatement(query);
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				namen.add(rs.getString(1));
			}

			stmt.executeQuery(query);
			for (int i = 0; i < namen.size(); i++) {
				if (namen.get(i).equals(name)) {
					return true; // Namen vergeben
				}
			}
			stmt.close();
		} catch (Exception e) {

		}
		return false;
	}
	
	public static boolean changeUser(User user) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog(ConfigManager.get("DB_DATABASE"));
			
			String query = "UPDATE `t_users` "
					+ "vorname = '?', " 		//1
					+ "nachname='?', "			//2		
					+ "F_Haarfarbe_Id=?, "		//3
					+ "F_Augenfarbe_Id=?, "		//4
					+ "F_Hobby_Id1=?, "			//5
					+ "F_Hobby_Id2=?, "			//6
					+ "F_Hobby_Id3=?, "			//7
					+ "F_Vorlieben_Haarfarbe=?,"//8
					+ "F_Vorlieben_Augenfarbe=?"//9
					+ "passwort=?, "			//10
					+ "bild=? "					//11
					+ "WHERE t_users = '?'";	//12
			stmt = con.prepareStatement(query);
			stmt.setString(1, user.getVorname());
			stmt.setString(2, user.getName());
			ArrayList<String> farbe = new ArrayList<>();
			int stelle= 0;
			farbe = HaarfarbeData.getAllHaarfarbe();
			for(int i = 0; i< farbe.size();i++) {
				if(user.getHaarfarbe().equals(farbe.get(i))) {
					stelle = i;
					stelle++;
					i = farbe.size();
				}
			}
			stmt.setInt(3, stelle);
			farbe.clear();
			farbe = AugenfarbeData.getAllAugemfarbe();
			for(int i = 0; i< farbe.size();i++) {
				if(user.getHaarfarbe().equals(farbe.get(i))) {
					stelle = i;
					stelle++;
					i = farbe.size();
				}
			}
			stmt.setInt(4, stelle);
			farbe.clear();
			ArrayList<String> hobbies = new ArrayList<>();
			hobbies = HobbiesData.getAllHobbies();
			for(int i = 0; i< hobbies.size();i++) {
				if(user.getHobby1().equals(hobbies.get(i))) {
					stelle = i;
					stelle++;
					i = hobbies.size();
				}
			}
			stmt.setInt(5, stelle);
			for(int i = 0; i< hobbies.size();i++) {
				if(user.getHobby2().equals(hobbies.get(i))) {
					stelle = i;
					stelle++;
					i = hobbies.size();
				}
			}
			stmt.setInt(6, stelle);
			for(int i = 0; i< hobbies.size();i++) {
				if(user.getHobby3().equals(hobbies.get(i))) {
					stelle = i;
					stelle++;
					i = hobbies.size();
				}
			}
			stmt.setInt(7, stelle);
			
			
			
		}catch(Exception e) {
			return false;
		}
		return false;
	}
}
