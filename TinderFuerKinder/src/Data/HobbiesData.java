package Data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

import Cofig.ConfigManager;

public class HobbiesData extends SQL{
	
	public static ArrayList<String> getAllHobbies() {
		Connection con = null;
		Statement stmt = null;
		ArrayList<String> hobbies = new ArrayList<>();
		
		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog(ConfigManager.get("DB_DATABASE"));
			
			String query = "SELECT hobby FROM T_Hobbies;";
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while(rs.next()) {
				hobbies.add(rs.getString(1));
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return hobbies;
		
	}
	
	public static boolean checkDB() {
		Connection con = null;
		Statement stmt = null;
		try { 
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog(ConfigManager.get("DB_DATABASE"));
			String query = "SELECT P_Hobbies_Id FROM T_Hobbies;";
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next()) {
				return true;
			}
		}catch (Exception e) {
			return false;
		}
		return false;
		
	}
	
}
