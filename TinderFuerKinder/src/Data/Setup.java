package Data;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.*;

import Cofig.ConfigManager;

public class Setup extends SQL {
	public boolean createDatabase() {
		Connection con = null;
		Statement stmt = null;
		
		
		try {
			con = getConnection();
			con.setAutoCommit(false); 
			
			String query1 = "CREATE DATABASE IF NOT EXISTS "+ConfigManager.get("DB_DATABASE");
			String[] tableQuerys = new String(Files.readAllBytes(Paths.get("resources/tables1.sql")), StandardCharsets.UTF_8).split(";");
			
			stmt = con.createStatement();
			stmt.execute(query1);
			con.setCatalog(ConfigManager.get("DB_DATABASE"));
			for(int i = 0;i < tableQuerys.length-1;i++) {
				stmt = con.createStatement();
				stmt.executeUpdate(tableQuerys[i]);
			}
			
			con.commit();
			return true;
		}catch(NumberFormatException |SQLException | IOException e) {
			e.printStackTrace();
			try {
				con.rollback();
			}catch(Exception e1) {
				return false;
			}finally {
				if(stmt != null) {
					try {
						stmt.close();
					}catch(Exception e2) {}
				}
				if(con != null) {
					try {
						con.close();
					}catch(Exception e3) {}
				}
			}
		}
		return false;
	}
	
	public boolean insertDemo() {
		return true;
	}
	
}
