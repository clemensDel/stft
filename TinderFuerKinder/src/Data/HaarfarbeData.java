package Data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import Cofig.ConfigManager;

public class HaarfarbeData extends SQL{
	
	public static ArrayList<String> getAllHaarfarbe() {
		Connection con = null;
		Statement stmt = null;
		ArrayList<String> farbe = new ArrayList<>();
		
		try {
			con = getConnection();
			con.setAutoCommit(false);
			con.setCatalog(ConfigManager.get("DB_DATABASE"));
			
			String query = "SELECT farbe FROM T_Haarfarbe;";
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while(rs.next()) {
				farbe.add(rs.getString(1));
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return farbe;
		
	}
}
