package TestClass;

import Data.*;

import java.awt.EventQueue;
import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;

import Cofig.*;
import Gui.*;
import Logic.*;

public class MainClass {

	private static Random r = new Random();

	public static void main(String[] args) {
		String path = "resources/TinderFuerKinder.properties";
		ArrayList<String> test = new ArrayList<>();

		ConfigManager.load(path); // Check ob DB existiert
		if (!HobbiesData.checkDB()) {
			ConfigManager.load(path);
			Setup setup = new Setup();
			String db = "Datenbank geladen: " + setup.createDatabase();
			String holder = "\n";
			for (int i = 0; i < db.length(); i++) {
				holder += "_";
			}
			System.out.println(db + holder);
		}
		addUser();
	
		
	}

	private static void addUser() {
		User u1 = new User();
		u1.setUsername("Timo");
		u1.setName("delbrueck");
		u1.setVorname("Clemens");
		u1.setAugenfarbe("Braun");
		u1.setHaarfarbe("Blond");
		u1.setHobby1("TEst1");
		u1.setHobby2("TEst2");
		u1.setHobby3("TEst3");
		u1.setVorliebeSpielplatz1("Test4");
		u1.setVorliebeSpielplatz2("Test5");
		u1.setVorliebeSpielplatz3("Test6");
		String salt = PasswordHash.getSalt();
		String passwort = "TEst";
		String newPasswort = PasswordHash.generateSecurePassword(passwort, salt);
		u1.setSalt(salt);
		u1.setPasswort(newPasswort);
		UserData.addUser(u1);
	}

	private static void openWindow() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui frame = new Gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
}
