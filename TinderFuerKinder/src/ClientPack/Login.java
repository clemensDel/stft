package ClientPack;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Login extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtIP;
	private JLabel lblIP;
	private JLabel lblPort;
	private JTextField txtPort;
	



	public Login() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		setResizable(false);
		setTitle("Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(300,380);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtName = new JTextField();
		txtName.setText("Tom");
		txtName.setBounds(66, 54, 160, 20);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setFont(new Font("Arial", Font.BOLD, 11));
		lblName.setBounds(67, 30, 160, 15);
		contentPane.add(lblName);
		
		txtIP = new JTextField();
		txtIP.setEnabled(true);
		txtIP.setEditable(true);
		txtIP.setText("localhost");
		txtIP.setBounds(66, 122, 160, 20);
		contentPane.add(txtIP);
		txtIP.setColumns(10);
		
		lblIP = new JLabel("IP-Adresse:");
		lblIP.setFont(new Font("Arial", Font.BOLD, 11));
		lblIP.setBounds(66, 97, 161, 14);
		contentPane.add(lblIP);
		
		lblPort = new JLabel("Port:");
		lblPort.setFont(new Font("Arial", Font.BOLD, 11));
		lblPort.setBounds(66, 166, 161, 14);
		contentPane.add(lblPort);
		
		txtPort = new JTextField();
		txtPort.setColumns(10);
		txtPort.setText("8192");
		txtPort.setBounds(66, 191, 160, 20);
		contentPane.add(txtPort);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int port = Integer.parseInt(txtPort.getText());
				login(txtName.getText(),txtIP.getText(),port);
			}
		});
		btnLogin.setBounds(146, 250, 80, 23);
		contentPane.add(btnLogin);
		
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnClose.setBounds(66, 250, 80, 23);
		contentPane.add(btnClose);
	}
	
	private void login(String name,String address,int port) {
		dispose();
		new ClientWindow(name,address,port);
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
