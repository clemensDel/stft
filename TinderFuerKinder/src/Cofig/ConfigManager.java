package Cofig;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.Properties;

public class ConfigManager {
	static Properties properties;
	
	public static boolean load(String path) {
		properties = new Properties();
		try {
			BufferedInputStream stream = new BufferedInputStream(new FileInputStream(path));
			properties.load(stream);
			stream.close();
		}catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public static String get(String key) {
		if(properties != null) {
			return properties.getProperty(key);
		}else {
			return null;
		}
	}
}
