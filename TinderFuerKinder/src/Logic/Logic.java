package Logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JOptionPane;

import Data.HobbiesData;
import Data.UserData;

public class Logic {

	public static boolean login(String name, String passwort) {
		
		return PasswordHash.verifyUserPassword(Data.UserData.getUser(name), passwort);		
	}

	public static void editUser(User user) {
		Data.UserData.changeUser(user);
	}
	
	public static boolean register(User newUser) {
		
		String attributes[] = {newUser.getUsername(), newUser.getVorname(),newUser.getName(), newUser.getVorliebeHaarfarbe(), 
							   newUser.getVorliebeAugenfarbe(), newUser.getHaarfarbe(),newUser.getAugenfarbe(), newUser.getHobby1(), 
							   newUser.getHobby2(), newUser.getHobby3(), newUser.getVorliebeSpielplatz1(), newUser.getVorliebeSpielplatz2(), newUser.getVorliebeSpielplatz3(),newUser.getPasswort()};
		
		for (int i = 0; i < attributes.length; i++) {
			System.out.println(attributes[i]);
			if (attributes[i] == "") {
				return false;
			}
		}
		
		String salt = PasswordHash.getSalt();
		
		newUser.setSalt(salt);
		String safePassword = PasswordHash.generateSecurePassword(newUser.getPasswort(), salt);
		newUser.setPasswort(safePassword);
		
			
		UserData.addUser(newUser);
		return true;
	}

	public static void like(String userName) { // https://www.google.com/imgres?imgurl=https%3A%2F%2Fcdn.shopify.com%2Fs%2Ffiles%2F1%2F1061%2F1924%2Fproducts%2FThumbs_Up_Hand_Sign_Emoji_large.png%3Fv%3D1480481047&imgrefurl=https%3A%2F%2Femojiisland.com%2Fproducts%2Fthumbs-up-hand-sign-emoji-icon&docid=LXl2kMcJXo3DbM&tbnid=9Xe1uknA9QX9CM%3A&vet=10ahUKEwjZ78Lx0b_eAhWdwAIHHV1bCB0QMwg-KAAwAA..i&w=480&h=480&client=firefox-b-ab&bih=1037&biw=1920&q=thumbsup&ved=0ahUKEwjZ78Lx0b_eAhWdwAIHHV1bCB0QMwg-KAAwAA&iact=mrc&uact=8

		 matchName(userName);
	}

	public static void dislike(String userName) { // https://www.google.com/imgres?imgurl=https%3A%2F%2Fcdn.shopify.com%2Fs%2Ffiles%2F1%2F1061%2F1924%2Fproducts%2FWhite_Thumbs_Down_Sign_Emoji_large.png%3Fv%3D1480481028&imgrefurl=https%3A%2F%2Femojiisland.com%2Fproducts%2Fwhite-thumbs-down-sign-emoji-icon&docid=vXIPeJL22qn7VM&tbnid=uzaQzxXpWbZmpM%3A&vet=10ahUKEwiDz_Tt0b_eAhXKzqQKHUTEDT4QMwg_KAEwAQ..i&w=480&h=480&client=firefox-b-ab&bih=1037&biw=1920&q=thumbs%20down&ved=0ahUKEwiDz_Tt0b_eAhXKzqQKHUTEDT4QMwg_KAEwAQ&iact=mrc&uact=8

		 matchName(userName);
	}

	public static String matchName(String userName) {
		Random rand = new Random();
		ArrayList<User> matchlist = new ArrayList();
		ArrayList<User> usercount = Data.UserData.getAllUser();
		
		User user1 = new User();
		
		for (int i = 0; i < usercount.size(); i++) {
			
			if (usercount.get(i).getUsername() == userName) {
				user1 = usercount.get(i);
			}
		}
		
		User user2 = new User();
		String randomMatch;
		
		ArrayList<String> hobbysUser1 = new ArrayList();
		hobbysUser1.add(user1.getHobby1());
		hobbysUser1.add(user1.getHobby2());
		hobbysUser1.add(user1.getHobby3());
		
		ArrayList<String> spielOrtUser1 = new ArrayList();
		spielOrtUser1.add(user1.getVorliebeSpielplatz1());
		spielOrtUser1.add(user1.getVorliebeSpielplatz2());
		spielOrtUser1.add(user1.getVorliebeSpielplatz3());
		
		ArrayList<String> hobbysUser2 = new ArrayList();;
		hobbysUser2.add(user2.getHobby1());
		hobbysUser2.add(user2.getHobby2());
		hobbysUser2.add(user2.getHobby3());
		
		ArrayList<String> spielOrtUser2 = new ArrayList();
		spielOrtUser2.add(user2.getVorliebeSpielplatz1());
		spielOrtUser2.add(user2.getVorliebeSpielplatz2());
		spielOrtUser2.add(user2.getVorliebeSpielplatz3());
		
		for (int i = 0; i < usercount.size()-1; i++) {
			
			user2 = usercount.get(i);
			for (int j = 0; j < 3; j++) {
				
				if ((hobbysUser1.get(i) == hobbysUser2.get(j)) && (spielOrtUser1.get(i) == spielOrtUser2.get(j)) &&
				(user1.getVorliebeHaarfarbe() == user2.getHaarfarbe() || user1.getVorliebeAugenfarbe() == user2.getAugenfarbe())) {
					matchlist.add(user2);
					
				}
			}
		}
		
		return randomMatch = matchlist.get(rand.nextInt(matchlist.size())).getVorname() + " " + matchlist.get(rand.nextInt(matchlist.size())).getName();
	}

	public static String matchPic() {

		return null;

	}
	
	public static String[] fillCbAugenfarbe() {
		String[] augenfarbe = {"Braun", "Gruen", "Blau"};
		
		return augenfarbe;
	}
	
	public static String[] fillCbHaarfarbe() {
		String[] Haarfarbe = {"Blond", "Schwarz", "Braun", "Rot", "Sonstiges"};
		
		return Haarfarbe;
	}
	
	public static String[] fillCbHobby() {
		String[] cbHobby = {"Basteln", "Malen", "Fotografieren", "Essen", "Shopping", "Raetsel loesen", "Wandern" , "Tanzen", "Fussball spielen" , "Schwimmen"};
		
		return cbHobby;
	}
	
	public static String[] fillCbVorliebeAugenfarbe() {
		String[] cbVorliebeAugenfarbe = {"Braun", "Gruen", "Blau"};
		
		return cbVorliebeAugenfarbe;
	}
	
	public static String[] fillCbVorliebeHaarfarbe() {
		String[] cbVorliebeHaarfarbe = {"Blond", "Schwarz", "Braun", "Rot", "Sonstiges"};
		
		return cbVorliebeHaarfarbe;
	}
	
	public static String[] fillCbSpielpl�tze() {
		String[] cbSpielpl�tze = {"Roseneck", "raebodep Spielplatz", "SuperduperRutschenSpielplatz", "Tulpenrund", "DerSpielplatz", "RundeEcken", "SpielSpa�Platz", "HerrTenbuschEhrenPlatz", "EsLebenDieKinder", "ArbeitsAufschiebePlatz"};
		
		return cbSpielpl�tze;
	}
	
	
}
