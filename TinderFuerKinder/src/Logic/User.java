package Logic;

import java.awt.Image;
import java.io.InputStream;

public class User {
	
	private String username,vorname,name,
	VorliebeSpielplatz1, VorliebeSpielplatz2, VorliebeSpielplatz3, VorliebeHaarfarbe,VorliebeAugenfarbe,
	haarfarbe,augenfarbe,Hobby1,Hobby2,Hobby3,
	salt,passwort;
	
	private Image image;

	
	

	public User() {
		
	}
	
	public User(String username,String vorname,String name, String VorliebeHaarfarbe, 
		   String VorliebeAugenfarbe, String VorliebeSpielplatz1, String VorliebeSpielplatz2, String VorliebeSpielplatz3, String haarfarbe, String augenfarbe, String Hobby1,
		   String Hobby2, String Hobby3,String salt,String passwort) {
		this.username = username;
		this.vorname = vorname;
		this.name = name;
		this.haarfarbe = haarfarbe;
		this.augenfarbe = augenfarbe;
		this.VorliebeHaarfarbe = VorliebeHaarfarbe;
		this.VorliebeAugenfarbe = VorliebeAugenfarbe;
		this.Hobby1 = Hobby1;
		this.Hobby2 = Hobby2;
		this.Hobby3 = Hobby3;
		this.salt = salt;
		this.passwort = passwort;
		this.image = image;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVorliebeHaarfarbe() {
		return VorliebeHaarfarbe;
	}

	public void setVorliebeHaarfarbe(String VorliebeHaarfarbe) {
		this.VorliebeHaarfarbe = VorliebeHaarfarbe;
	}
	
	public String getVorliebeAugenfarbe() {
		return VorliebeAugenfarbe;
	}

	public void setVorliebeAugenfarbe(String VorliebeAugenfarbe) {
		this.VorliebeAugenfarbe = VorliebeAugenfarbe;
	}

	public String getHaarfarbe() {
		return haarfarbe;
	}

	public void setHaarfarbe(String haarfarbe) {
		this.haarfarbe = haarfarbe;
	}

	public String getAugenfarbe() {
		return augenfarbe;
	}

	public void setAugenfarbe(String augenfarbe) {
		this.augenfarbe = augenfarbe;
	}
	
	public String getVorliebeSpielplatz1() {
		return VorliebeSpielplatz1;
	}

	public void setVorliebeSpielplatz1(String VorliebeSpielplatz1) {
		this.VorliebeSpielplatz1 = VorliebeSpielplatz1;
	}
	
	public String getVorliebeSpielplatz2() {
		return VorliebeSpielplatz2;
	}

	public void setVorliebeSpielplatz2(String VorliebeSpielplatz2) {
		this.VorliebeSpielplatz2 = VorliebeSpielplatz2;
	}
	
	public String getVorliebeSpielplatz3() {
		return VorliebeSpielplatz3;
	}

	public void setVorliebeSpielplatz3(String VorliebeSpielplatz3) {
		this.VorliebeSpielplatz3 = VorliebeSpielplatz3;
	}

	public String getHobby1() {
		return Hobby1;
	}

	public void setHobby1(String Hobby1) {
		this.Hobby1 = Hobby1;
	}
	
	public String getHobby2() {
		return Hobby2;
	}

	public void setHobby2(String Hobby2) {
		this.Hobby2 = Hobby2;
	}
	
	public String getHobby3() {
		return Hobby3;
	}

	public void setHobby3(String Hobby3) {
		this.Hobby3 = Hobby3;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}

	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}

	public String getSalt() {
		return salt;
	}

	public String getPasswort() {
		return passwort;
	}

	public Image getImage() {
		return image;
	}

	public void setInput(Image image) {
		this.image = image;
	}
	
}
