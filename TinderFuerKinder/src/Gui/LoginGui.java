package Gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class LoginGui extends JFrame {

	private JPanel contentPane,loginPanel,signupPanel;
	private JTextField txtUsername;
	private JPasswordField txtPassword;
	private JLabel lblPassword;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginGui frame = new LoginGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	

	
	
	public LoginGui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Login");
		setSize(300,230);
		setResizable(false);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		loginPanel = new JPanel();
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		loginPanel.setLayout(null);
		
		contentPane.add(loginPanel, BorderLayout.CENTER);
		txtUsername = new JTextField();
		txtUsername.setBounds(66, 54, 160, 20);
		loginPanel.add(txtUsername);
		txtUsername.setColumns(10);
		
		JLabel lblUsername = new JLabel("Name:");
		lblUsername.setFont(new Font("Arial", Font.BOLD, 11));
		lblUsername.setBounds(67, 30, 160, 15);
		loginPanel.add(lblUsername);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(66, 122, 160, 20);
		loginPanel.add(txtPassword);
		txtPassword.setColumns(10);
		
		lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("Arial", Font.BOLD, 11));
		lblPassword.setBounds(66, 97, 161, 14);
		loginPanel.add(lblPassword, BorderLayout.WEST);
		
		JButton btn_Signup = new JButton("Sign up");
		btn_Signup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Sign Up");
				setSize(300,230);
				loginPanel.setVisible(false);
				
			}
		});
		btn_Signup.setBounds(146, 150, 80, 23);
		loginPanel.add(btn_Signup);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Login");
				setSize(400,300);
			}
		});
		btnLogin.setBounds(66, 150, 80, 23);
		loginPanel.add(btnLogin);
	}

}
