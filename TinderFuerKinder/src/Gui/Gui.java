package Gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import ClientPack.Login;
import Logic.Logic;
import Logic.User;

import java.awt.CardLayout;
import java.awt.Panel;

import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ScrollPaneConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.nio.file.Paths;
import java.awt.event.ActionEvent;
import java.awt.ScrollPane;
import java.awt.Checkbox;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import java.awt.Dimension;
import java.awt.Choice;
import javax.swing.JPasswordField;

public class Gui extends JFrame {
	private String username;
	private JPanel contentPane;
	private JTextField textBenutzername;
	private JTextField textPasswort_b;
	private final JLabel lblNewLabel_4 = new JLabel("Benutzername");
	private JTextField textBenutzername_b;
	private JTextField textVorname;
	private JTextField textNachname;
	private JTextField textChat;
	private JPasswordField textPasswort;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) { 
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui frame = new Gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Gui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 735, 672);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		setLocationRelativeTo(null);

		Panel login = new Panel();
		contentPane.add(login, "first_card");

		JLabel lblNewLabel = new JLabel("Benutzername");

		textBenutzername = new JTextField();
		textBenutzername.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("Passwort");
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				username = textBenutzername.getText(); 
				if(Logic.login(textBenutzername.getText(), textPasswort.getText())) {
					textBenutzername.setText("") ;
					textPasswort.setText("") ;
				((CardLayout)contentPane.getLayout()).show(contentPane, "second_card");
				}else {JOptionPane.showMessageDialog(contentPane,
                        "Benutzername oder Passwort ist falsch!",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);}
				
			}
		});

		JButton btnRegistrierung = new JButton("Registrierung");
		btnRegistrierung.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				((CardLayout)contentPane.getLayout()).show(contentPane, "second_card");
			}
		});
		
		textPasswort = new JPasswordField();
		GroupLayout gl_login = new GroupLayout(login);
		gl_login.setHorizontalGroup(
			gl_login.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_login.createSequentialGroup()
					.addGap(75)
					.addGroup(gl_login.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_login.createSequentialGroup()
							.addGroup(gl_login.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_login.createSequentialGroup()
									.addComponent(lblNewLabel_1, GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)
									.addGap(428))
								.addGroup(gl_login.createSequentialGroup()
									.addComponent(btnLogin, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
									.addGap(99)
									.addComponent(btnRegistrierung))
								.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 334, GroupLayout.PREFERRED_SIZE))
							.addGap(55))
						.addGroup(gl_login.createSequentialGroup()
							.addGroup(gl_login.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(textBenutzername, Alignment.LEADING)
								.addComponent(textPasswort, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 330, Short.MAX_VALUE))
							.addContainerGap())))
		);
		gl_login.setVerticalGroup(
			gl_login.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_login.createSequentialGroup()
					.addGap(46)
					.addComponent(lblNewLabel, GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(textBenutzername, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(118)
					.addComponent(lblNewLabel_1)
					.addGap(35)
					.addComponent(textPasswort, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(113)
					.addGroup(gl_login.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnLogin, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnRegistrierung, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addGap(42))
		);
		login.setLayout(gl_login);

		JPanel erstellung = new JPanel();
		JScrollPane jErstellung = new JScrollPane(erstellung);
		GridBagLayout gbl_erstellung = new GridBagLayout();
		gbl_erstellung.columnWidths = new int[] { 0, 0, 0, 157, 0, 0, 18, 0 };
		gbl_erstellung.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_erstellung.columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_erstellung.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		erstellung.setLayout(gbl_erstellung);
		GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
		gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_4.gridx = 1;
		gbc_lblNewLabel_4.gridy = 0;
		erstellung.add(lblNewLabel_4, gbc_lblNewLabel_4);
		
		textBenutzername_b = new JTextField();
		GridBagConstraints gbc_textBenutzername_b = new GridBagConstraints();
		gbc_textBenutzername_b.insets = new Insets(0, 0, 5, 5);
		gbc_textBenutzername_b.fill = GridBagConstraints.HORIZONTAL;
		gbc_textBenutzername_b.gridx = 1;
		gbc_textBenutzername_b.gridy = 1;
		erstellung.add(textBenutzername_b, gbc_textBenutzername_b);
		textBenutzername_b.setColumns(10);
		
		
		
		

		JLabel lblPasswort = new JLabel("Passwort");
		GridBagConstraints gbc_lblPasswort = new GridBagConstraints();
		gbc_lblPasswort.insets = new Insets(0, 0, 5, 5);
		gbc_lblPasswort.gridx = 1;
		gbc_lblPasswort.gridy = 2;
		erstellung.add(lblPasswort, gbc_lblPasswort);

		textPasswort_b = new JTextField();
		GridBagConstraints gbc_textPasswort_b = new GridBagConstraints();
		gbc_textPasswort_b.insets = new Insets(0, 0, 5, 5);
		gbc_textPasswort_b.fill = GridBagConstraints.HORIZONTAL;
		gbc_textPasswort_b.gridx = 1;
		gbc_textPasswort_b.gridy = 3;
		erstellung.add(textPasswort_b, gbc_textPasswort_b);
		textPasswort_b.setColumns(10);
														
														JLabel lblVorname = new JLabel("Name");
														GridBagConstraints gbc_lblVorname = new GridBagConstraints();
														gbc_lblVorname.insets = new Insets(0, 0, 5, 5);
														gbc_lblVorname.gridx = 1;
														gbc_lblVorname.gridy = 4;
														erstellung.add(lblVorname, gbc_lblVorname);
														
														JLabel lblNachname = new JLabel("Nachname");
														GridBagConstraints gbc_lblNachname = new GridBagConstraints();
														gbc_lblNachname.insets = new Insets(0, 0, 5, 5);
														gbc_lblNachname.gridx = 3;
														gbc_lblNachname.gridy = 4;
														erstellung.add(lblNachname, gbc_lblNachname);
														
														textVorname = new JTextField();
														GridBagConstraints gbc_textVorname = new GridBagConstraints();
														gbc_textVorname.insets = new Insets(0, 0, 5, 5);
														gbc_textVorname.fill = GridBagConstraints.HORIZONTAL;
														gbc_textVorname.gridx = 1;
														gbc_textVorname.gridy = 5;
														erstellung.add(textVorname, gbc_textVorname);
														textVorname.setColumns(10);
														
														textNachname = new JTextField();
														GridBagConstraints gbc_textNachname = new GridBagConstraints();
														gbc_textNachname.insets = new Insets(0, 0, 5, 5);
														gbc_textNachname.fill = GridBagConstraints.HORIZONTAL;
														gbc_textNachname.gridx = 3;
														gbc_textNachname.gridy = 5;
														erstellung.add(textNachname, gbc_textNachname);
														textNachname.setColumns(10);
												
														JLabel lblAugenfarbe = new JLabel("Augenfarbe");
														GridBagConstraints gbc_lblAugenfarbe = new GridBagConstraints();
														gbc_lblAugenfarbe.insets = new Insets(0, 0, 5, 5);
														gbc_lblAugenfarbe.gridx = 1;
														gbc_lblAugenfarbe.gridy = 7;
														erstellung.add(lblAugenfarbe, gbc_lblAugenfarbe);
										
												JComboBox augenfarbe = new JComboBox();
												augenfarbe.setModel(new DefaultComboBoxModel(Logic.fillCbAugenfarbe()));
												
												GridBagConstraints gbc_augenfarbe = new GridBagConstraints();
												gbc_augenfarbe.insets = new Insets(0, 0, 5, 5);
												gbc_augenfarbe.fill = GridBagConstraints.HORIZONTAL;
												gbc_augenfarbe.gridx = 1;
												gbc_augenfarbe.gridy = 8;
												erstellung.add(augenfarbe, gbc_augenfarbe);
								
										JLabel lblHaarfarbe = new JLabel("Haarfarbe");
										GridBagConstraints gbc_lblHaarfarbe = new GridBagConstraints();
										gbc_lblHaarfarbe.insets = new Insets(0, 0, 5, 5);
										gbc_lblHaarfarbe.gridx = 1;
										gbc_lblHaarfarbe.gridy = 9;
										erstellung.add(lblHaarfarbe, gbc_lblHaarfarbe);
						
								JComboBox haarfarbe = new JComboBox();
								haarfarbe.setModel(new DefaultComboBoxModel(Logic.fillCbHaarfarbe()));
								GridBagConstraints gbc_haarfarbe = new GridBagConstraints();
								gbc_haarfarbe.insets = new Insets(0, 0, 5, 5);
								gbc_haarfarbe.fill = GridBagConstraints.HORIZONTAL;
								gbc_haarfarbe.gridx = 1;
								gbc_haarfarbe.gridy = 10;
								erstellung.add(haarfarbe, gbc_haarfarbe);
				
						JLabel lblHobbys = new JLabel("Hobbys");
						GridBagConstraints gbc_Hobbys = new GridBagConstraints();
						gbc_Hobbys.gridwidth = 2;
						gbc_Hobbys.insets = new Insets(0, 0, 5, 5);
						gbc_Hobbys.gridx = 3;
						gbc_Hobbys.gridy = 11;
						erstellung.add(lblHobbys, gbc_Hobbys);
		
				JComboBox hobby_1 = new JComboBox();
				hobby_1.setModel(new DefaultComboBoxModel(Logic.fillCbHobby()));
				GridBagConstraints gbc_hobby_1 = new GridBagConstraints();
				gbc_hobby_1.insets = new Insets(0, 0, 5, 5);
				gbc_hobby_1.fill = GridBagConstraints.HORIZONTAL;
				gbc_hobby_1.gridx = 1;
				gbc_hobby_1.gridy = 12;
				erstellung.add(hobby_1, gbc_hobby_1);
		
				JComboBox hobby_2 = new JComboBox();
				hobby_2.setModel(new DefaultComboBoxModel(Logic.fillCbHobby()));
				hobby_2.setSize(new Dimension(1000000, 2147483647));
				GridBagConstraints gbc_hobby_2 = new GridBagConstraints();
				gbc_hobby_2.insets = new Insets(0, 0, 5, 5);
				gbc_hobby_2.fill = GridBagConstraints.HORIZONTAL;
				gbc_hobby_2.gridx = 3;
				gbc_hobby_2.gridy = 12;
				erstellung.add(hobby_2, gbc_hobby_2);
		
				JComboBox hobby_3 = new JComboBox();
				hobby_3.setModel(new DefaultComboBoxModel(Logic.fillCbHobby()));
				GridBagConstraints gbc_hobby_3 = new GridBagConstraints();
				gbc_hobby_3.gridwidth = 3;
				gbc_hobby_3.insets = new Insets(0, 0, 5, 0);
				gbc_hobby_3.fill = GridBagConstraints.HORIZONTAL;
				gbc_hobby_3.gridx = 4;
				gbc_hobby_3.gridy = 12;
				erstellung.add(hobby_3, gbc_hobby_3);
		
		JLabel lblNewLabel_2 = new JLabel("Spielpl\u00E4tze");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.gridwidth = 2;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 3;
		gbc_lblNewLabel_2.gridy = 15;
		erstellung.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		JComboBox Spielplatz_1 = new JComboBox();
		Spielplatz_1.setModel(new DefaultComboBoxModel(Logic.fillCbSpielpl�tze()));
		GridBagConstraints gbc_Spielplatz_1 = new GridBagConstraints();
		gbc_Spielplatz_1.insets = new Insets(0, 0, 5, 5);
		gbc_Spielplatz_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_Spielplatz_1.gridx = 1;
		gbc_Spielplatz_1.gridy = 16;
		erstellung.add(Spielplatz_1, gbc_Spielplatz_1);
		
		JComboBox Spielplatz_2 = new JComboBox();
		Spielplatz_2.setModel(new DefaultComboBoxModel(Logic.fillCbSpielpl�tze()));
		GridBagConstraints gbc_Spielplatz_2 = new GridBagConstraints();
		gbc_Spielplatz_2.insets = new Insets(0, 0, 5, 5);
		gbc_Spielplatz_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_Spielplatz_2.gridx = 3;
		gbc_Spielplatz_2.gridy = 16;
		erstellung.add(Spielplatz_2, gbc_Spielplatz_2);
		
		JComboBox Spielplatz_3 = new JComboBox();
		Spielplatz_3.setModel(new DefaultComboBoxModel(Logic.fillCbSpielpl�tze()));
		GridBagConstraints gbc_Spielplatz_3 = new GridBagConstraints();
		gbc_Spielplatz_3.gridwidth = 3;
		gbc_Spielplatz_3.insets = new Insets(0, 0, 5, 0);
		gbc_Spielplatz_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_Spielplatz_3.gridx = 4;
		gbc_Spielplatz_3.gridy = 16;
		erstellung.add(Spielplatz_3, gbc_Spielplatz_3);
		
		JLabel lblNewLabel_3 = new JLabel("Vorlieben:");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 3;
		gbc_lblNewLabel_3.gridy = 17;
		erstellung.add(lblNewLabel_3, gbc_lblNewLabel_3);
		
		JLabel lblAugenfarbe_1 = new JLabel("Augenfarbe");
		GridBagConstraints gbc_lblAugenfarbe_1 = new GridBagConstraints();
		gbc_lblAugenfarbe_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblAugenfarbe_1.gridx = 3;
		gbc_lblAugenfarbe_1.gridy = 19;
		erstellung.add(lblAugenfarbe_1, gbc_lblAugenfarbe_1);
		
		JComboBox vAugenfarbe = new JComboBox();
		vAugenfarbe.setModel(new DefaultComboBoxModel(Logic.fillCbVorliebeAugenfarbe()));
		GridBagConstraints gbc_vAugenfarbe = new GridBagConstraints();
		gbc_vAugenfarbe.insets = new Insets(0, 0, 5, 5);
		gbc_vAugenfarbe.fill = GridBagConstraints.HORIZONTAL;
		gbc_vAugenfarbe.gridx = 3;
		gbc_vAugenfarbe.gridy = 20;
		erstellung.add(vAugenfarbe, gbc_vAugenfarbe);
		
		JLabel lblHaarfarbe_1 = new JLabel("Haarfarbe");
		GridBagConstraints gbc_lblHaarfarbe_1 = new GridBagConstraints();
		gbc_lblHaarfarbe_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblHaarfarbe_1.gridx = 3;
		gbc_lblHaarfarbe_1.gridy = 21;
		erstellung.add(lblHaarfarbe_1, gbc_lblHaarfarbe_1);
		
		JComboBox vHaarfarbe = new JComboBox();
		vHaarfarbe.setModel(new DefaultComboBoxModel(Logic.fillCbVorliebeHaarfarbe()));
		GridBagConstraints gbc_vHaarfarbe = new GridBagConstraints();
		gbc_vHaarfarbe.insets = new Insets(0, 0, 5, 5);
		gbc_vHaarfarbe.fill = GridBagConstraints.HORIZONTAL;
		gbc_vHaarfarbe.gridx = 3;
		gbc_vHaarfarbe.gridy = 22;
		erstellung.add(vHaarfarbe, gbc_vHaarfarbe);
		
		JButton btnZumBearbeitenDruecken = new JButton("zum bearbeiten druecken");
		btnZumBearbeitenDruecken.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				User user3 = Data.UserData.getUser(username);
				textBenutzername_b.setEditable(false);
				textBenutzername_b.setText("nicht aenderbar!");
				textVorname.setText(user3.getVorname() + "");
				textNachname.setText(user3.getName() + "");
				augenfarbe.setSelectedItem(user3.getAugenfarbe());
				haarfarbe.setSelectedItem(user3.getHaarfarbe());
				hobby_1.setSelectedItem(user3.getHobby1());
				hobby_2.setSelectedItem(user3.getHobby2());
				hobby_3.setSelectedItem(user3.getHobby3());
				Spielplatz_1.setSelectedItem(user3.getVorliebeSpielplatz1());
				Spielplatz_2.setSelectedItem(user3.getVorliebeSpielplatz2());
				Spielplatz_3.setSelectedItem(user3.getVorliebeSpielplatz3());
				vAugenfarbe.setSelectedItem(user3.getVorliebeAugenfarbe());
				vHaarfarbe.setSelectedItem(user3.getVorliebeHaarfarbe());
			}
		});
		GridBagConstraints gbc_btnZumBearbeitenDruecken = new GridBagConstraints();
		gbc_btnZumBearbeitenDruecken.insets = new Insets(0, 0, 5, 5);
		gbc_btnZumBearbeitenDruecken.gridx = 3;
		gbc_btnZumBearbeitenDruecken.gridy = 1;
		erstellung.add(btnZumBearbeitenDruecken, gbc_btnZumBearbeitenDruecken);
		
	
		
		JButton btnSpeichern = new JButton("speichern");
		btnSpeichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				User user2 = Data.UserData.getUser(username);
				user2.setUsername(username);
				user2.setPasswort(textPasswort_b.getText());
				user2.setVorname(textVorname.getText());
				user2.setName(textNachname.getText());
				user2.setAugenfarbe(augenfarbe.getSelectedItem().toString());
				user2.setHaarfarbe(haarfarbe.getSelectedItem().toString());;
				user2.setVorliebeSpielplatz1(Spielplatz_1.getSelectedItem().toString());
				user2.setVorliebeSpielplatz2(Spielplatz_2.getSelectedItem().toString());
				user2.setVorliebeSpielplatz3(Spielplatz_3.getSelectedItem().toString());
				user2.setHobby1(hobby_1.getSelectedItem().toString());
				user2.setHobby2(hobby_2.getSelectedItem().toString());
				user2.setHobby3(hobby_3.getSelectedItem().toString());
				user2.setVorliebeAugenfarbe(vAugenfarbe.getSelectedItem().toString());
				user2.setVorliebeHaarfarbe(vHaarfarbe.getSelectedItem().toString());
			}
		});
		
		JButton btnErstellen = new JButton("erstellen");
		btnErstellen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				User user = new User();
				user.setUsername(textBenutzername_b.getText());
				user.setPasswort(textPasswort_b.getText());
				user.setVorname(textVorname.getText());
				user.setName(textNachname.getText());
				user.setAugenfarbe(augenfarbe.getSelectedItem().toString());
				user.setHaarfarbe(haarfarbe.getSelectedItem().toString());;
				user.setVorliebeSpielplatz1(Spielplatz_1.getSelectedItem().toString());
				user.setVorliebeSpielplatz2(Spielplatz_2.getSelectedItem().toString());
				user.setVorliebeSpielplatz3(Spielplatz_3.getSelectedItem().toString());
				user.setHobby1(hobby_1.getSelectedItem().toString());
				user.setHobby2(hobby_2.getSelectedItem().toString());
				user.setHobby3(hobby_3.getSelectedItem().toString());
				user.setVorliebeAugenfarbe(vAugenfarbe.getSelectedItem().toString());
				user.setVorliebeHaarfarbe(vHaarfarbe.getSelectedItem().toString());
				Logic.register(user);
			}
		});
		
		GridBagConstraints gbc_btnErstellen = new GridBagConstraints();
		gbc_btnErstellen.insets = new Insets(0, 0, 5, 5);
		gbc_btnErstellen.gridx = 1;
		gbc_btnErstellen.gridy = 24;
		erstellung.add(btnErstellen, gbc_btnErstellen);
		GridBagConstraints gbc_btnSpeichern = new GridBagConstraints();
		gbc_btnSpeichern.insets = new Insets(0, 0, 5, 5);
		gbc_btnSpeichern.gridx = 2;
		gbc_btnSpeichern.gridy = 24;
		erstellung.add(btnSpeichern, gbc_btnSpeichern);
		
		JButton btnLogout = new JButton("Logout");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout)contentPane.getLayout()).show(contentPane, "first_card");
			}
		});
		
		JButton btnLike_Dislike = new JButton("Like/Dislike");
		btnLike_Dislike.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout)contentPane.getLayout()).show(contentPane, "third_card");
			}
		});
		
		JButton btnChat_1 = new JButton("Chat");
		btnChat_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout)contentPane.getLayout()).show(contentPane, "fourth_card");
			}
		});
		GridBagConstraints gbc_btnChat_1 = new GridBagConstraints();
		gbc_btnChat_1.insets = new Insets(0, 0, 0, 5);
		gbc_btnChat_1.gridx = 4;
		gbc_btnChat_1.gridy = 25;
		erstellung.add(btnChat_1, gbc_btnChat_1);
		GridBagConstraints gbc_btnLike_Dislike = new GridBagConstraints();
		gbc_btnLike_Dislike.insets = new Insets(0, 0, 0, 5);
		gbc_btnLike_Dislike.gridx = 5;
		gbc_btnLike_Dislike.gridy = 25;
		erstellung.add(btnLike_Dislike, gbc_btnLike_Dislike);
		GridBagConstraints gbc_btnLogout = new GridBagConstraints();
		gbc_btnLogout.gridx = 6;
		gbc_btnLogout.gridy = 25;
		erstellung.add(btnLogout, gbc_btnLogout);
		jErstellung.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		contentPane.add(jErstellung, "second_card");

		Panel like_disslike = new Panel();
		contentPane.add(like_disslike, "third_card");
		
		
		JLabel lblBeschreibung = new JLabel("Hobbys: ");
		
		JLabel lblBILDPLATZHALTER = new JLabel("BILD!!!!!!!!!!!!");
		
		try {                            
            Image kazoo = ImageIO.read(getClass().getResource("kazoo.jpg"));
            Image newimg = kazoo.getScaledInstance( 200, 200,  java.awt.Image.SCALE_SMOOTH ) ; 
            lblBILDPLATZHALTER.setIcon(new ImageIcon(newimg));

          } catch (Exception ex) {
            System.out.println(ex);
          }
		
		
		
		JButton btnDisslike = new JButton("");
		btnDisslike.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
				});
		
		try {                            
            Image disslike = ImageIO.read(getClass().getResource("dislike.png"));
            Image newimg = disslike.getScaledInstance( 20, 20,  java.awt.Image.SCALE_SMOOTH ) ; 
            btnDisslike.setIcon(new ImageIcon(newimg));

          } catch (Exception ex) {
            System.out.println(ex);
          }
		
		

	    
		
		JButton btnLike = new JButton("");
		btnLike.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				
			}
				});
		
		try {                            
            Image like = ImageIO.read(getClass().getResource("like.png"));
            Image newimg = like.getScaledInstance( 20, 20,  java.awt.Image.SCALE_SMOOTH ) ; 
            btnLike.setIcon(new ImageIcon(newimg));

          } catch (Exception ex) {
            System.out.println(ex);
          }
		
		JButton btnLogout_1 = new JButton("Logout");
		btnLogout_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout)contentPane.getLayout()).show(contentPane, "first_card");
			}
		});
		
		JButton btnChat = new JButton("Chat");
		btnChat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				System.out.println("Test");
			}
		});
		
		JButton btnProfil = new JButton("Profil");
		btnProfil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout)contentPane.getLayout()).show(contentPane, "second_card");
			}
		});
		
		GroupLayout gl_like_disslike = new GroupLayout(like_disslike);
		gl_like_disslike.setHorizontalGroup(
			gl_like_disslike.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_like_disslike.createSequentialGroup()
					.addGap(182)
					.addComponent(lblBeschreibung, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(237, Short.MAX_VALUE))
				.addGroup(gl_like_disslike.createSequentialGroup()
					.addGap(114)
					.addComponent(btnDisslike, GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 275, Short.MAX_VALUE)
					.addComponent(btnLike, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
					.addGap(134))
				.addGroup(gl_like_disslike.createSequentialGroup()
					.addContainerGap(545, Short.MAX_VALUE)
					.addComponent(btnProfil)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnChat)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnLogout_1))
				.addGroup(Alignment.LEADING, gl_like_disslike.createSequentialGroup()
					.addGap(244)
					.addComponent(lblBILDPLATZHALTER, GroupLayout.PREFERRED_SIZE, 192, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(298, Short.MAX_VALUE))
		);
		gl_like_disslike.setVerticalGroup(
			gl_like_disslike.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_like_disslike.createSequentialGroup()
					.addContainerGap(166, Short.MAX_VALUE)
					.addComponent(lblBILDPLATZHALTER, GroupLayout.PREFERRED_SIZE, 172, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(lblBeschreibung, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
					.addGap(125)
					.addGroup(gl_like_disslike.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnLike)
						.addComponent(btnDisslike))
					.addGap(44)
					.addGroup(gl_like_disslike.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnLogout_1)
						.addComponent(btnChat)
						.addComponent(btnProfil)))
		);
		like_disslike.setLayout(gl_like_disslike);
		 
		Panel chat = new Panel();
		contentPane.add(chat, "fourth_card");
		
		JLabel lblChat = new JLabel("Chat");
		
		textChat = new JTextField();
		textChat.setColumns(10);
		
		JButton btnLogout_2 = new JButton("Logout");
		btnLogout_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout)contentPane.getLayout()).show(contentPane, "first_card");
			}
		});
		
		JButton btnLike_Dislike_2 = new JButton("Like/Dislike");
		btnLike_Dislike_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout)contentPane.getLayout()).show(contentPane, "third_card");
			}
		});
		
		JButton btnProfil_2 = new JButton("Profil");
		btnProfil_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((CardLayout)contentPane.getLayout()).show(contentPane, "second_card");
			}
		});
		
		JButton btnSenden = new JButton("Senden!");
		btnSenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblChat.setText("Du: " + textChat.getText());
				textChat.setText("");
			}
		});
		GroupLayout gl_chat = new GroupLayout(chat);
		gl_chat.setHorizontalGroup(
			gl_chat.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_chat.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblChat, GroupLayout.DEFAULT_SIZE, 724, Short.MAX_VALUE))
				.addComponent(textChat, GroupLayout.DEFAULT_SIZE, 734, Short.MAX_VALUE)
				.addGroup(gl_chat.createSequentialGroup()
					.addGap(270)
					.addComponent(btnSenden, GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE)
					.addGap(343))
				.addGroup(gl_chat.createSequentialGroup()
					.addGap(484)
					.addComponent(btnProfil_2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnLike_Dislike_2, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnLogout_2, GroupLayout.DEFAULT_SIZE, 65, Short.MAX_VALUE)
					.addGap(33))
		);
		gl_chat.setVerticalGroup(
			gl_chat.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_chat.createSequentialGroup()
					.addComponent(lblChat, GroupLayout.DEFAULT_SIZE, 253, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(textChat, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnSenden, GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
					.addGap(193)
					.addGroup(gl_chat.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(btnLike_Dislike_2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnProfil_2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnLogout_2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
		);
		chat.setLayout(gl_chat);

	}
}


