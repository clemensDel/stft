
 CREATE TABLE IF NOT EXISTS T_Haarfarbe(
 P_Haarfarbe_Id INT PRIMARY KEY AUTO_INCREMENT,
 farbe VARCHAR(10)
 );
 INSERT INTO T_Haarfarbe (P_Haarfarbe_Id,farbe) VALUES(1,'Blond');
 INSERT INTO T_Haarfarbe (P_Haarfarbe_Id,farbe) VALUES(2,'Schwarz');
 INSERT INTO T_Haarfarbe (P_Haarfarbe_Id,farbe) VALUES(3,'Braun');
 INSERT INTO T_Haarfarbe (P_Haarfarbe_Id,farbe) VALUES(4,'Rot');
 INSERT INTO T_Haarfarbe (P_Haarfarbe_Id,farbe) VALUES(5,'Sonstiges');
 CREATE TABLE IF NOT EXISTS T_Augenfarbe(
 P_Augenfarbe_Id INT PRIMARY KEY AUTO_INCREMENT,
 farbe VARCHAR(10)
 );
 CREATE TABLE IF NOT EXISTS T_Spielplatz(
P_Spielplatz_Id INT PRIMARY KEY AUTO_INCREMENT,
name TEXT
);
 INSERT INTO T_Spielplatz (P_Spielplatz_Id,name) VALUE (1,'Roseneck');
 INSERT INTO T_Spielplatz (P_Spielplatz_Id,name) VALUE (2,'raebodep Spielplatz');
 INSERT INTO T_Spielplatz (P_Spielplatz_Id,name) VALUE (3,'SuperduperRutschenSpielplatz');
 INSERT INTO T_Spielplatz (P_Spielplatz_Id,name) VALUE (4,'Tulpenrund');
 INSERT INTO T_Spielplatz (P_Spielplatz_Id,name) VALUE (5,'DerSpielplatz');
 INSERT INTO T_Spielplatz (P_Spielplatz_Id,name) VALUE (6,'RundeEcken');
 INSERT INTO T_Spielplatz (P_Spielplatz_Id,name) VALUE (7,'SpielSpaßPlatz');
 INSERT INTO T_Spielplatz (P_Spielplatz_Id,name) VALUE (8,'HerrTenbuschEhrenPlatz');
 INSERT INTO T_Spielplatz (P_Spielplatz_Id,name) VALUE (9,'EsLebenDieKinder');
 INSERT INTO T_Spielplatz (P_Spielplatz_Id,name) VALUE (10,'ArbeitsAufschiebePlatz');
 INSERT INTO T_Augenfarbe (P_Augenfarbe_Id,farbe) VALUES (1,'Braun'); 
 INSERT INTO T_Augenfarbe (P_Augenfarbe_Id,farbe) VALUES (2,'Gruen'); 
 INSERT INTO T_Augenfarbe (P_Augenfarbe_Id,farbe) VALUES (3,'Blau');
 CREATE TABLE IF NOT EXISTS T_Hobbies(
 P_Hobbies_Id INT PRIMARY KEY AUTO_INCREMENT,
 hobby VARCHAR(20)
 );
 INSERT INTO T_Hobbies (P_Hobbies_Id, hobby) VALUES (1,'Basteln');
 INSERT INTO T_Hobbies (P_Hobbies_Id, hobby) VALUES (2,'Malen');
 INSERT INTO T_Hobbies (P_Hobbies_Id, hobby) VALUES (3,'Fotografieren');
 INSERT INTO T_Hobbies (P_Hobbies_Id, hobby) VALUES (4,'Essen');
 INSERT INTO T_Hobbies (P_Hobbies_Id, hobby) VALUES (5,'Shopping');
 INSERT INTO T_Hobbies (P_Hobbies_Id, hobby) VALUES (6,'Raetsel loesen');
 INSERT INTO T_Hobbies (P_Hobbies_Id, hobby) VALUES (7,'Wandern');
 INSERT INTO T_Hobbies (P_Hobbies_Id, hobby) VALUES (8,'Tanzen');
 INSERT INTO T_Hobbies (P_Hobbies_Id, hobby) VALUES (9,'Fussball spielen');
 INSERT INTO T_Hobbies (P_Hobbies_Id, hobby) VALUES (10,'Schwinnen');
 INSERT INTO T_Hobbies (P_Hobbies_Id, hobby) VALUES (11,'NULL');
 CREATE TABLE IF NOT EXISTS T_Users(
P_User_Id INT PRIMARY KEY AUTO_INCREMENT,
username VARCHAR(30) NOT NULL,
vorname VARCHAR(20) NOT NULL,
nachname VARCHAR(20) NOT NULL,
F_Haarfarbe_Id INT NOT NULL,
FOREIGN KEY (F_Haarfarbe_Id) REFERENCES T_Haarfarbe(P_Haarfarbe_Id),
F_Augenfarbe_Id INT NOT NULL,
FOREIGN KEY (F_Augenfarbe_Id) REFERENCES T_Augenfarbe(P_Augenfarbe_Id),
F_Hobby_Id1 INT,
FOREIGN KEY (F_Hobby_Id1) REFERENCES T_Hobbies(P_Hobbies_Id),
F_Hobby_Id2 INT,
FOREIGN KEY (F_Hobby_Id2) REFERENCES T_Hobbies(P_Hobbies_Id),
F_Hobby_Id3 INT,
FOREIGN KEY (F_Hobby_Id3) REFERENCES T_Hobbies(P_Hobbies_Id),
F_Vorlieben_Haarfarbe INT,
F_Vorlieben_Augenfarbe INT,
FOREIGN KEY (F_Vorlieben_Haarfarbe) REFERENCES T_Haarfarbe(P_Haarfarbe_Id),
FOREIGN KEY (F_Vorlieben_Augenfarbe) REFERENCES T_Augenfarbe(P_Augenfarbe_Id),
salt VARCHAR(64),
passwort VARCHAR(64),
bild BLOB,
F_Spielplatz1 INT,
F_Spielplatz2 INT,
F_Spielplatz3 INT,
FOREIGN KEY (F_Spielplatz1) REFERENCES T_Spielplatz(P_Spielplatz_Id),
FOREIGN KEY (F_Spielplatz2) REFERENCES T_Spielplatz(P_Spielplatz_Id),
FOREIGN KEY (F_Spielplatz3) REFERENCES T_Spielplatz(P_Spielplatz_Id)
 );
 INSERT INTO T_Users (P_User_Id,username,vorname,nachname,F_Augenfarbe_Id,F_Haarfarbe_Id,F_Hobby_Id1,F_Hobby_Id2,F_Hobby_Id3,F_Vorlieben_Haarfarbe,F_Vorlieben_Augenfarbe,salt,passwort,bild,F_Spielplatz1,F_Spielplatz2,F_Spielplatz3)
 VALUES(				1,		'TimoBrain','Timo','Schaedel',2				,1			,1				,2			,	3,				1			,1,		null,null,	null,	null,null,null);
 INSERT INTO T_Users (P_User_Id,username,vorname,nachname,F_Augenfarbe_Id,F_Haarfarbe_Id,F_Hobby_Id1,F_Hobby_Id2,F_Hobby_Id3,F_Vorlieben_Haarfarbe,F_Vorlieben_Augenfarbe,salt,passwort,bild,F_Spielplatz1,F_Spielplatz2,F_Spielplatz3)
 VALUES(			  2,	'ClemensMaster','Clemens','Delbrueck',2,1,1,1,2,3,1,null,null,null,null,null,null);
 INSERT INTO T_Users (P_User_Id,username,vorname,nachname,F_Augenfarbe_Id,F_Haarfarbe_Id,F_Hobby_Id1,F_Hobby_Id2,F_Hobby_Id3,F_Vorlieben_Haarfarbe,F_Vorlieben_Augenfarbe,salt,passwort,bild,F_Spielplatz1,F_Spielplatz2,F_Spielplatz3)
 VALUES(3,'PaulBlackWhite','Paul','Nartschik',2,1,1,1,2,3,1,null,null,null,null,null,null);
CREATE TABLE IF NOT EXISTS T_Uservoting(
P_Vote_ID INT PRIMARY KEY AUTO_INCREMENT,
F_User_Voting INT,
F_User_Voting2 INT,
Votetyp INT,
FOREIGN KEY (F_User_Voting) REFERENCES T_Users(P_User_Id),
FOREIGN KEY (F_User_Voting2) REFERENCES T_Users(P_User_Id)
);
CREATE TABLE IF NOT EXISTS T_Match(
P_Match_Id INT PRIMARY KEY AUTO_INCREMENT,
F_User_Match INT,
F_User_Match2 INT,
FOREIGN KEY (F_User_Match) REFERENCES T_Users(P_User_Id),
FOREIGN KEY (F_User_Match2) REFERENCES T_Users(P_User_Id)
);
CREATE TABLE IF NOT EXISTS T_Dislike(
P_Dislike_ID INT PRIMARY KEY AUTO_INCREMENT,
F_User_Dislike INT,
F_User_Dislike2 INT,
FOREIGN KEY (F_User_Dislike) REFERENCES T_Users(P_User_Id),
FOREIGN KEY (F_User_Dislike2) REFERENCES T_Users(P_User_Id)
);
CREATE TABLE IF NOT EXISTS T_Blacklist(
P_Blacklist_Id INT PRIMARY KEY AUTO_INCREMENT,
F_Match_Id INT,
FOREIGN KEY (F_Match_Id) REFERENCES T_Match(P_Match_Id)
);
