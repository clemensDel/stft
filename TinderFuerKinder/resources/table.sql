CREATE TABLE IF NOT EXISTS T_Users(
P_User_name VARCHAR(30) PRIMARY KEY,
vorname VARCHAR(30),
name VARCHAR(30),
augenfarbe VARCHAR(30),
haarfarbe VARCHAR(20),
hobby1 VARCHAR(30),
hobby2 VARCHAR(30),
hobby3 VARCHAR(30),
spielplatz1 VARCHAR(30),
spielplatz2 VARCHAR(30),
spielplatz3 VARCHAR(30),
password VARCHAR(64),
salt VARCHAR(64)
);
